document.addEventListener("DOMContentLoaded", function () {
    window.log("DOM content loaded.");
});

function click1() {
    var f1 = document.getElementsByName("field1");
    var f2 = document.getElementsByName("field2");
    var r = document.getElementById("result");
    var x = f1[0].value * f2[0].value;

    r.innerHTML = (
        x.toString().match(/^[0-9]+$/) === null
        ? "Некорректный ввод"
        : "Общая стоимость равна " + x + " руб."
    );

    return false;
}